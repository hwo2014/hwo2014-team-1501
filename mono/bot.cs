using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using Newtonsoft.Json;

/// <summary>
/// Bot (our driver).
/// </summary>
public class Bot
{
	/// <summary>
	/// The writer used to send messages to the server.
	/// </summary>
	private StreamWriter writer;
	
	private CarId ourCar;
	
	private CarPositions[] carPositions;

    /// <summary>
    /// The race data, created on GameInit message.
    /// </summary>
    private Race raceData;
    
	/// <summary>
	/// The current throttle.
	/// </summary>
	private float currentThrottle = 1.0f;

    /// <summary>
    /// Distance on tile at previous tick.
    /// </summary>
    private float previousDistOnTile = 0.0f;

    /// <summary>
    /// Current distance on tile.
    /// </summary>
    private float currentDistOnTile = 0.0f;
        
    /// <summary>
    /// The velocity on the previous tick.
    /// </summary>
    private float previousVelocity = 0.0f;

    /// <summary>
    /// Current velovity (tileDist/tick^2).
    /// </summary>
    private float currentVelocity = 0.0f;
        
    /// <summary>
    /// Current acceleration.
    /// </summary>
    private float currentAcceleration = 0.0f;

    /// <summary>
    /// The current piece the car is on.
    /// </summary>
    private int currentPiece = 0;

    /// <summary>
    /// The next piece the car will be on.
    /// </summary>
    private int nextPiece = 1;

    /// <summary>
    /// The piece after that (don't judge the name).
    /// </summary>
    private int nextNextPiece = 2;

    /// <summary>
    /// The index of the next corner piece.
    /// </summary>
    private int nextCorner;

    /// <summary>
    /// The velocity we should be at when we reach the next corner.
    /// </summary>
    private float maxVelocityAtNextCorner;

    /// <summary>
    /// The drag coefficient.
    /// </summary>
    private float dragCoefficient;

    /// <summary>
    /// The drag coefficient.
    /// </summary>
    private float centripetalForce;

    /// <summary>
    /// The distance it'll take to slow down.
    /// </summary>
    private float distToSlow;

    /// <summary>
    /// The distance to the turn.
    /// </summary>
    private float distToTurn;
    
    /// <summary>
    /// The previous velocity that isn't 0.
    /// </summary>
    private float previousMovingVelocity;

    /// <summary>
    /// The current lane the car is on.
    /// </summary>
    private int currentLane = 0;

	/// <summary>
	/// Assesses the car positions.
	/// </summary>
	/// <param name='msg'>
	/// Message passed from the server.
	/// </param>
	private void AssessCarPositions (MsgWrapper msg)
	{
		carPositions = JsonConvert.DeserializeObject<CarPositions[]>(msg.data.ToString());

        for (int i = 0; i < carPositions.Length; i++)
        {
            if (ourCar.Color == carPositions[i].Id.Color)
            {
                currentPiece = carPositions[i].PiecePosition.PieceIndex;
                nextPiece = (raceData.Track.Pieces.Length - 1 == currentPiece) ? 0 : currentPiece + 1;
                nextNextPiece = (raceData.Track.Pieces.Length - 1 == nextPiece) ? 0 : nextPiece + 1;
                currentLane = carPositions[i].PiecePosition.Lane.StartLaneIndex;

                previousDistOnTile = currentDistOnTile;
                currentDistOnTile = carPositions[i].PiecePosition.InPieceDistance;

                if (currentDistOnTile >= previousDistOnTile)
                {                    
                    previousVelocity = currentVelocity;
                    if(currentVelocity != 0)
                    {
                        previousMovingVelocity = currentVelocity;
                    }
                    currentVelocity = currentDistOnTile - previousDistOnTile;                    
                }

                break;
            }
        }

        for (int i = currentPiece; i < raceData.Track.Pieces.Length; i++ )
        {
            if(raceData.Track.Pieces[i].Angle != 0)
            {
                nextCorner = i;
                break;
            }
        }

        Console.WriteLine("{0} : {1}", carPositions[0].PiecePosition.PieceIndex, carPositions[0].PiecePosition.InPieceDistance);
	}

	/// <summary>
	/// Logs the car crash.
	/// </summary>
	/// <param name='msg'>
	/// Message from the server.
	/// </param>
	private void LogCrash(MsgWrapper msg){

		CarId carId = JsonConvert.DeserializeObject<CarId> (msg.data.ToString());
        if (raceData.Track.Pieces[currentPiece].Angle != 0)
        {
            float currentCentripetalForce = (previousMovingVelocity * previousMovingVelocity) / (raceData.Track.Pieces[currentPiece].Radius - raceData.Track.Lanes[currentLane].DistanceFromCenter);
            centripetalForce = currentCentripetalForce < centripetalForce || centripetalForce == 0 ? currentCentripetalForce : centripetalForce;
            System.Diagnostics.Debug.WriteLine("CRASHCRASHCRASH");
            System.Diagnostics.Debug.WriteLine(centripetalForce);
        }
		Console.WriteLine ("{0} has crashed", carId.Name);
        System.Diagnostics.Debug.WriteLine("CRASHCRASHCRASH");
        System.Diagnostics.Debug.WriteLine(centripetalForce);
		// If car is me then mybe slow down on this bend.
	}

	/// <summary>
	/// Send the specified msg to the server.
	/// </summary>
	/// <param name='msg'>
	/// Message to send.
	/// </param>
	private void Send (SendMsg msg)
	{
		writer.WriteLine (msg.ToJson ());
	}


	/// <summary>
	/// Think placeholder (currently switches to optimal segment on reaching switches).
	/// </summary>
	/// <param name='msg'>
	/// Message passed from the server.
	/// </param>
	private void Think (MsgWrapper msg)
	{
        LaneUpdate();
        AnalysisStuff();
        AdjustSpeed();     
	}

    private void AnalysisStuff()
    {
        

        if(nextCorner >= currentPiece)
        {
            distToTurn = raceData.Track.Pieces[currentPiece].Length - currentDistOnTile;
            for (int i = currentPiece + 1; i == nextCorner; i++)
            {
                distToTurn += raceData.Track.Pieces[i].Length;
            }
        }
        else
        {
            distToTurn = float.MaxValue;
        }
        
        if (centripetalForce != 0)
        {
            maxVelocityAtNextCorner = (float)Math.Sqrt(centripetalForce / (raceData.Track.Pieces[nextCorner].Radius - raceData.Track.Lanes[currentLane].DistanceFromCenter));
            distToSlow = util.distanceToReduceSpeed(currentVelocity, maxVelocityAtNextCorner * 60, dragCoefficient * 0.1f);
            
            return;
        }
    }

    private void LaneUpdate()
    {
        // Check if the next piece is any of the switch indices.
        int switchSegment = 0;
        bool onSwitch = false;

        for (int i = 0; i < raceData.Track.SwitchIndices.Length; i++)
        {
            if (nextPiece == raceData.Track.SwitchIndices[i])
            {
                switchSegment = i;
                onSwitch = true;
                break;
            }
        }

        // If we're not about to hit a switch, don't bother.
        if (!onSwitch)
        {
            return;
        }

        //Deal with the case where we're on either the inner or outer lane.
        if (currentLane == 0 || currentLane == raceData.Track.Lanes.Length - 1)
        {
            float currentLaneLength = raceData.Track.TrackSegmentLengths[switchSegment][currentLane];
            float adjacentLane1;
            int adjacentLane1Index;

            if (currentLane == 0)
            {
                adjacentLane1 = raceData.Track.TrackSegmentLengths[switchSegment][currentLane + 1];
                adjacentLane1Index = currentLane + 1;
            }
            else
            {
                adjacentLane1 = raceData.Track.TrackSegmentLengths[switchSegment][currentLane - 1];
                adjacentLane1Index = currentLane - 1;
            }

            if (adjacentLane1 >= currentLaneLength)
            {
                return;
            }

            int adjacentLaneDistanceFromCenter = raceData.Track.Lanes[currentLane].DistanceFromCenter;
            int currentLaneDistanceFromCenter = raceData.Track.Lanes[adjacentLane1Index].DistanceFromCenter;

            if (adjacentLaneDistanceFromCenter < currentLaneDistanceFromCenter)
            {
                Send(new SwitchLane("Right"));
            }
            else
            {
                Send(new SwitchLane("Left"));
            }

            return;
        }

        // Can't really test this until we get a > 2 lane track, so expect explosions.
        float leftLaneLength = (raceData.Track.Lanes[currentLane - 1] != null && raceData.Track.Lanes[currentLane - 1].DistanceFromCenter < raceData.Track.Lanes[currentLane].DistanceFromCenter)
            ? raceData.Track.TrackSegmentLengths[switchSegment][currentLane - 1] : raceData.Track.TrackSegmentLengths[switchSegment][currentLane + 1];

        float rightLaneLength = (raceData.Track.Lanes[currentLane - 1] != null && raceData.Track.Lanes[currentLane - 1].DistanceFromCenter > raceData.Track.Lanes[currentLane].DistanceFromCenter)
            ? raceData.Track.TrackSegmentLengths[switchSegment][currentLane - 1] : raceData.Track.TrackSegmentLengths[switchSegment][currentLane + 1];

        if (rightLaneLength < leftLaneLength && rightLaneLength < raceData.Track.TrackSegmentLengths[switchSegment][currentLane])
        {
            Send(new SwitchLane("Right"));
        }
        else if (leftLaneLength < rightLaneLength && leftLaneLength < raceData.Track.TrackSegmentLengths[switchSegment][currentLane])
        {
            Send(new SwitchLane("Left"));
        }   
    }

    private void AdjustSpeed()
    {
        System.Diagnostics.Debug.WriteLine(currentThrottle);
        System.Diagnostics.Debug.WriteLine(dragCoefficient);
        System.Diagnostics.Debug.WriteLine(distToSlow);
        System.Diagnostics.Debug.WriteLine(currentVelocity);
        System.Diagnostics.Debug.WriteLine("Max Velocity: " + maxVelocityAtNextCorner);

        if (raceData.Track.Pieces[currentPiece].Angle > 25 || raceData.Track.Pieces[currentPiece].Angle < -25)
        {
            HoldSpeed();
            //currentThrottle = 0.64f;
            return;
        }             

        if (distToTurn <= distToSlow && distToTurn > 0 && distToSlow > 0)
        {
            currentThrottle = 0.0f;
            return;
        }

        if (raceData.Track.Pieces[currentPiece].Angle != 0 && raceData.Track.Pieces[nextPiece].Angle == 0 && currentDistOnTile > (raceData.Track.Pieces[currentPiece].Length / 6) * 5)
        {
            currentThrottle = 0.8f;
            return;
        } 
        currentThrottle = 1.0f;        
    }

    private void HoldSpeed()
    {
        if(currentVelocity > maxVelocityAtNextCorner * 90)
        {
            currentThrottle = 0.5f;
        }
        else
        {
            currentThrottle = 0.7f;
        }
    }

	/// <summary>
	/// The entry point of the program, where the program control starts and ends.
	/// </summary>
	/// <param name='args'>
	/// The command-line arguments.
	/// </param>
	public static void Main (string[] args)
	{
		string host = args [0];
		int port = int.Parse (args [1]);
		string botName = args [2];
		string botKey = args [3];

		Console.WriteLine ("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
		try {
			using (TcpClient client = new TcpClient(host, port)) {
				NetworkStream stream = client.GetStream ();
				StreamReader reader = new StreamReader (stream);
				StreamWriter writer = new StreamWriter (stream);
				writer.AutoFlush = true;

                string country = "keimola";

# if(DEBUG)

                Console.WriteLine("Select Country:\n 1: Finland\n 2: Germany\n 3: USA\n 4: France");
                switch (Console.ReadKey().KeyChar.ToString())
                {
                    case "1":
                        country = "keimola";
                        break;
                    case "2":
                        country = "germany";
                        break;
                    case "3":
                        country = "usa";
                        break;
                    case "4":
                        country = "france";
                        break;
                }
                
                new Bot(reader, writer, new JoinRace(new BotId(botName, botKey), country, "rawr", 1));

# else
                new Bot(reader, writer, new Join(botName,botKey));
# endif
			}
		} catch (Exception ex) {
			Console.WriteLine (ex.Message);
		}
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Bot"/> class.
	/// </summary>
	/// <param name='reader'>
	/// Reader which is used to recieve from the server.
	/// </param>
	/// <param name='writer'>
	/// Writer which is used to respond to the server.
	/// </param>
	/// <param name='joinMsg'>
	/// Join is the joinMsg race message used to start a race.
	/// </param>
	Bot (StreamReader reader, StreamWriter writer, SendMsg joinMsg)
	{
		this.writer = writer;
		string line;

		Send (joinMsg);

        bool stageOne = true, stageTwo = true, stageThree = true;

		while ((line = reader.ReadLine()) != null) {
			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper> (line);
			switch (msg.msgType) {

			case "crash":
				LogCrash(msg);
				Send (new Ping ());
				break;

			case "carPositions":
                AssessCarPositions (msg);
                if(stageOne)
                {
                    Send(new Throttle(1.0f));
                    stageOne = false;
                    break;
                }
                else if(stageTwo)
                {
                    if (currentVelocity != previousVelocity)
                    {
                        stageTwo = false;
                    }
                    Send(new Throttle(1.0f));
                    break;
                }			
                else if(stageThree)
                {
                    dragCoefficient = (2 * previousVelocity - currentVelocity) / (float) Math.Pow(previousVelocity, 2);
                    stageThree = false;
                    break;
                }
				Think (msg);
				Send (new Throttle (currentThrottle));
				break;

			case "join":
				Console.WriteLine ("Joined");
				Send (new Ping ());
				break;

			case "gameInit":
				Console.WriteLine ("Race init");
                System.Diagnostics.Debug.WriteLine(msg.data);
                Game gameData = JsonConvert.DeserializeObject<Game>(msg.data.ToString());
                raceData = gameData.Race;
                util.AssessTrack(raceData);
				Send (new Ping ());
				break;

			case "gameEnd":
				Console.WriteLine ("Race ended");
				Send (new Ping ());
				break;

			case "gameStart":
				Console.WriteLine ("Race starts");
				Send (new Ping ());
				break;
				
			case "yourCar":
				ourCar = JsonConvert.DeserializeObject<CarId>(msg.data.ToString());
				Console.WriteLine("Our car has joined and is {0}", ourCar.Color);
				break;
				
			default:				
				Send (new Ping ());
				break;
			}
		}
	}
}
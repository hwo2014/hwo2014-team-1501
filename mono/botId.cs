﻿using System;
using System.Text;

/// <summary>
/// The Bot ID class.
/// </summary>
public class BotId
{
    /// <summary>
    /// The bot name.
    /// </summary>
    public string name;

    /// <summary>
    /// The bot key.
    /// </summary>
    public string key;

    /// <summary>
    /// Initializes a new instance of the <see cref="BotId"/> class.
    /// </summary>
    /// <param name="name">The name.</param>
    /// <param name="key">The key.</param>
    public BotId(string name, string key)
    {
        this.name = name;
        this.key = key;
    }
}
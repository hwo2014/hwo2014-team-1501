using System;

public class Car
{
	public CarId Id;
	public CarDimensions Dimensions;
		
	public Car (CarId id, CarDimensions	dimensions)
	{
		Id = id;
		Dimensions = dimensions;		
	}
}
using System;

/// <summary>
/// Car Dimensions.
/// </summary>
public struct CarDimensions
{
    /// <summary>
    /// The length.
    /// </summary>
	public float Length;

    /// <summary>
    /// The width.
    /// </summary>
	public float Width;

    /// <summary>
    /// The position of the guide flag.
    /// </summary>
	public float GuideFlagPosition;
}
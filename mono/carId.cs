using System;

/// <summary>
/// Car.
/// </summary>
public class CarId{

	/// <summary>
	/// The name of the car.
	/// </summary>
	public string Name;

	/// <summary>
	/// The color of the car.
	/// </summary>
	public string Color;

	/// <summary>
	/// The piece position.
	/// </summary>
	//public PiecePosition PiecePosition;

	/// <summary>
	/// Initializes a new instance of the <see cref="Car"/> class.
	/// </summary>
	/// <param name='name'>
	/// Name of car.
	/// </param>
	/// <param name='color'>
	/// Color of car.
	/// </param>
	public CarId (string name, string color)
	{
		Name = name;
		Color = color;
	}
}


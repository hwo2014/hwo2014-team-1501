using System;

public class CarPositions
{
	public CarId Id;
	public float Angle;
	public PiecePosition PiecePosition;
	
	public CarPositions (CarId id, float angle, PiecePosition piecePosition)
	{
		Id = id;
		Angle = angle;
		PiecePosition = piecePosition;
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Join race message.
/// </summary>
public class JoinRace : SendMsg
{

    /// <summary>
    /// The Bot ID.
    /// </summary>
    public BotId botId;

    /// <summary>
    /// The track name.
    /// </summary>
    public string trackName;

    /// <summary>
    /// The password.
    /// </summary>
    public string password;

    /// <summary>
    /// The car count.
    /// </summary>
    public uint carCount;

    /// <summary>
    /// The override for message type.
    /// </summary>
    /// <returns>The message type.</returns>
    protected override string MsgType()
    {
        return "joinRace";
    }

    /// <summary>
    /// Initialises a new instance of class <see cref="JoinRace"/>.
    /// </summary>
    /// <param name="botId">Bot ID.</param>
    /// <param name="trackName">Track name.</param>
    /// <param name="password">Password.</param>
    /// <param name="carCount">Car count.</param>
    public JoinRace(BotId botId, string trackName, string password, uint carCount)
    {
        this.botId = botId;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }
}
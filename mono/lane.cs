using System;

public class Lane
{
	/// <summary>
	/// The distance from center.
	/// </summary>
	public int DistanceFromCenter;
	
	/// <summary>
	/// The index.
	/// </summary>
	public int Index;
	
	/// <summary>
	/// Initializes a new instance of the <see cref="Lane"/> class.
	/// </summary>
	/// <param name='distanceFromCenter'>
	/// Distance from center line.
	/// </param>
	/// <param name='index'>
	/// Index.
	/// </param>
	public Lane (int distanceFromCenter, int index)
	{
		DistanceFromCenter = distanceFromCenter;
		Index = index;
	}
}


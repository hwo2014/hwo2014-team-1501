using System;
 
/// <summary>
/// Location.
/// </summary>
public class Location
{	
	/// <summary>
	/// Point.
	/// </summary>
	public class Point
	{		
		/// <summary>
		/// The x.
		/// </summary>
		public float X;
		
		/// <summary>
		/// The y.
		/// </summary>
		public float Y;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="Location.Point"/> class.
		/// </summary>
		/// <param name='x'>
		/// X.
		/// </param>
		/// <param name='y'>
		/// Y.
		/// </param>
		public Point (float x, float y)
		{
			
			X = x;
			Y = y;
		}
	}
	
	/// <summary>
	/// The position.
	/// </summary>
	public Point Position;
	
	/// <summary>
	/// The angle.
	/// </summary>
	public float Angle;
	
	/// <summary>
	/// Initializes a new instance of the <see cref="Location"/> class.
	/// </summary>
	/// <param name='position'>
	/// Position.
	/// </param>
	/// <param name='angle'>
	/// Angle.
	/// </param>
	public Location (Point position, float angle)
	{
		Position = position;
		Angle = angle;
	}
}
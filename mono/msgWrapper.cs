﻿using System;

/// <summary>
/// Message wrapper.
/// </summary>
class MsgWrapper
{
    /// <summary>
    /// The type of the message.
    /// </summary>
    public string msgType;

    /// <summary>
    /// The data, needs to be casted depending on message type.
    /// </summary>
    public Object data;

    /// <summary>
    /// Initializes a new instance of the <see cref="MsgWrapper"/> class.
    /// </summary>
    /// <param name='msgType'>
    /// Message type.
    /// </param>
    /// <param name='data'>
    /// Data.
    /// </param>
    public MsgWrapper(string msgType, Object data)
    {
        this.msgType = msgType;
        this.data = data;
    }
}

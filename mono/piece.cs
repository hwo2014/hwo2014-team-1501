using System;

/// <summary>
/// Piece.
/// </summary>
public class Piece
{
	/// <summary>
	/// The length.
	/// </summary>
	public float Length;
	
	/// <summary>
	/// Whether the piece has a lane switch.
	/// </summary>
	public bool Switch;
	
	/// <summary>
	/// The radius.
	/// </summary>
	public float Radius;
	
	/// <summary>
	/// The angle of the piece if curved.
	/// </summary>
	public float Angle;
	
	/// <summary>
	/// Initializes a new instance of the <see cref="Piece"/> class.
	/// </summary>
    /// <param name="length">The length.</param>
	/// <param name='radius'>Radius.</param>
	/// <param name='angle'>Angle.</param>
	/// <param name='Switch'>Switch.</param>
	public Piece (float length = 0, float radius = 0, float angle = 0, bool Switch = false)
	{
        this.Switch = Switch;

        if (length != 0)
        {
            Length = length;            
            return;
        }

        Radius = radius;
        Angle = angle;
        Length = 0;		
	}
}
using System;

/// <summary>
/// Piece position.
/// </summary>
public class PiecePosition
{
	/// <summary>
	/// The index of the piece.
	/// </summary>
	public int PieceIndex;
	
	/// <summary>
	/// The in piece distance.
	/// </summary>
	public float InPieceDistance;
	
	/// <summary>
	/// The lane.
	/// </summary>
	public LaneIndex Lane;
	
	/// <summary>
	/// The lap index.
	/// </summary>
	public int Lap;
	
	/// <summary>
	/// Initializes a new instance of the <see cref="PiecePosition"/> class.
	/// </summary>
	/// <param name='pieceIndex'>
	/// Piece index.
	/// </param>
	/// <param name='inPieceDistance'>
	/// In piece distance.
	/// </param>
	/// <param name='lane'>
	/// Lane switch index pair.
	/// </param>
	/// <param name='lap'>
	/// Lap index.
	/// </param>
	public PiecePosition (int pieceIndex, float inPieceDistance, LaneIndex lane, int lap)
	{
		PieceIndex = pieceIndex;
		InPieceDistance = inPieceDistance;
		Lane = lane;
		Lap = lap;
	}
}
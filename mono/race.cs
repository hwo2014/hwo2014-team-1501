using System;

/// <summary>
/// Race.
/// </summary>
public class Race{
	
	/// <summary>
	/// The track.
	/// </summary>
	public Track Track;
	
	/// <summary>
	/// The cars.
	/// </summary>
	public Car[] Cars;
	
	/// <summary>
	/// The race session.
	/// </summary>
	public RaceSession RaceSession;	

	/// <summary>
	/// Initializes a new instance of the <see cref="Race"/> class.
	/// </summary>
	/// <param name='track'>
	/// Track definition.
	/// </param>
	/// <param name='cars'>
	/// Array of cars.
	/// </param>
	/// <param name='raceSession'>
	/// Race session.
	/// </param>
	public Race (Track track, Car[] cars, RaceSession raceSession)
	{
		Track = track;
		Cars = cars;
		RaceSession = raceSession;
	}
}
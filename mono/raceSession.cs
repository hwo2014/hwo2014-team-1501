using System;

/// <summary>
/// Race session.
/// </summary>
public class RaceSession
{
	/// <summary>
	/// The number of laps.
	/// </summary>
	public int Laps;

	/// <summary>
	/// The max lap time in ms.
	/// </summary>
	public int MaxLapTimeMs;

	/// <summary>
	/// Determines if this is a quick race.
	/// </summary>
	public bool QuickRace;

	/// /// <summary>
	/// Initializes a new instance of the <see cref="Race"/> class.
	/// </summary>
	/// <param name='laps'>
	/// Number of laps.
	/// </param>
	/// <param name='maxLapTimeMs'>
	/// Max lap time in ms.
	/// </param>
	/// <param name='quickRace'>
	/// Quick race value.
	/// </param>
	public RaceSession (int laps, int maxLapTimeMs, bool quickRace)
	{
		Laps = laps;
		MaxLapTimeMs = maxLapTimeMs;
		QuickRace = quickRace;
	}
}


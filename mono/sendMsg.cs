﻿using System;
using Newtonsoft.Json;

/// <summary>
/// Send message.
/// </summary>
public abstract class SendMsg
{
    /// <summary>
    /// Converts message to JSON format.
    /// </summary>
    /// <returns>
    /// String format of JSON message.
    /// </returns>
    public string ToJson()
    {
        return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
    }

    /// <summary>
    /// Messages the data.
    /// </summary>
    /// <returns>
    /// The data as object.
    /// </returns>
    protected virtual Object MsgData()
    {
        return this;
    }

    /// <summary>
    /// Abstract message type.
    /// </summary>
    /// <returns>
    /// The type as string.
    /// </returns>
    protected abstract string MsgType();
}

/// <summary>
/// Join message.
/// </summary>
class Join : SendMsg
{
    /// <summary>
    /// The name.
    /// </summary>
    public string name;

    /// <summary>
    /// The key.
    /// </summary>
    public string key;

    /// <summary>
    /// The color.
    /// </summary>
    public string color;

    /// <summary>
    /// Initializes a new instance of the <see cref="Join"/> class.
    /// </summary>
    /// <param name='name'>
    /// Name of car.
    /// </param>
    /// <param name='key'>
    /// Key from team page.
    /// </param>
    public Join(string name, string key)
    {
        this.name = name;
        this.key = key;
        this.color = "red";
    }

    /// <summary>
    /// Override of the message type.
    /// </summary>
    /// <returns>
    /// The type as string.
    /// </returns>
    protected override string MsgType()
    {
        return "join";
    }
}

/// <summary>
/// Ping message.
/// </summary>
class Ping : SendMsg
{
    /// <summary>
    /// Override of the message type.
    /// </summary>
    /// <returns>
    /// The type as string.
    /// </returns>
    protected override string MsgType()
    {
        return "ping";
    }
}

/// <summary>
/// Throttle message.
/// </summary>
class Throttle : SendMsg
{
    /// <summary>
    /// The value of the throttle.
    /// </summary>
    public double value;

    /// <summary>
    /// Initializes a new instance of the <see cref="Throttle"/> class.
    /// </summary>
    /// <param name='value'>
    /// Value of throttle.
    /// </param>
    public Throttle(double value)
    {
        this.value = value;
    }

    /// <summary>
    /// Override of the messages data.
    /// </summary>
    /// <returns>
    /// The data as Object.
    /// </returns>
    protected override Object MsgData()
    {
        return this.value;
    }

    /// <summary>
    /// Override of the message type.
    /// </summary>
    /// <returns>
    /// The type as string.
    /// </returns>
    protected override string MsgType()
    {
        return "throttle";
    }
}

/// <summary>
/// Lane switch message.
/// </summary>
class SwitchLane : SendMsg
{
    /// <summary>
    /// The direction to turn.
    /// </summary>
    public string direction;

    /// <summary>
    /// Initializes a new instance of the <see cref="SwitchLane"/> class.
    /// </summary>
    /// <param name='direction'>
    /// Direction of lane switch.
    /// </param>
    public SwitchLane(string direction)
    {
        this.direction = direction;
    }

    /// <summary>
    /// Override of the messages data.
    /// </summary>
    /// <returns>
    /// The data as Object.
    /// </returns>
    protected override Object MsgData()
    {
        return this.direction;
    }

    /// <summary>
    /// Override of the message type.
    /// </summary>
    /// <returns>
    /// The type as string.
    /// </returns>
    protected override string MsgType()
    {
        return "switchLane";
    }
}

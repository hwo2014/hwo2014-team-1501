using System;

/// <summary>
/// Track.
/// </summary>
public class Track
{
	/// <summary>
	/// The identifier.
	/// </summary>
	public string Id;
	
	/// <summary>
	/// The name.
	/// </summary>
	public string Name;
	
	/// <summary>
	/// The pieces.
	/// </summary>
	public Piece[] Pieces;
	
	/// <summary>
	/// The lanes.
	/// </summary>
	public Lane[] Lanes;
	
	/// <summary>
	/// The starting point.
	/// </summary>
	public Location StartingPoint;

    /// <summary>
    /// The indices for each switch.
    /// </summary>
    public int[] SwitchIndices;

    /// <summary>
    /// The track segment lengths by lane.
    /// </summary>
    public float[][] TrackSegmentLengths;

	/// <summary>
	/// Initializes a new instance of the <see cref="Track"/> class.
	/// </summary>
	/// <param name='id'>
	/// Identifier.
	/// </param>
	/// <param name='name'>
	/// Name.
	/// </param>
	/// <param name='pieces'>
	/// Pieces.
	/// </param>
	/// <param name='lanes'>
	/// Lanes.
	/// </param>
	/// <param name='startingPoint'>
	/// Starting point.
	/// </param>
	public Track (string id, string name, Piece[] pieces, Lane[] lanes, Location startingPoint)
	{
		Id = id;
		Name = name;
		Pieces = pieces;
		Lanes = lanes;
		StartingPoint = startingPoint;
	}
}



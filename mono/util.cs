﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// The utilities class.
/// </summary>
public static class util
{
    /// <summary>
    /// Calculate the lengths of each section of track between pairs of switches.
    /// </summary>
    /// <param name="raceData">The data for the race.</param>
    public static void AssessTrack(Race raceData)
    {
        // First, get the indices where each switch occurs.
        int laneCount = raceData.Track.Lanes.Length;
        List<int> switchIndices = raceData.Track.Pieces.Select((piece, index) => new {index, piece}).Where(item => item.piece.Switch).Select(item => item.index).ToList();

        // Reorganise the array of track pieces into something less annoying to work with.
        List<Piece[]> switchSegments = new List<Piece[]>();
        List<Piece> segment;

        for(int i = 0; i < switchIndices.Count - 1; i++)
        {
            segment = new List<Piece>();

            for(int j = switchIndices[i] + 1; j < switchIndices[i+1]; j++)
            {
                segment.Add(raceData.Track.Pieces[j]);
            }

            switchSegments.Add(segment.ToArray());
        }

        // The final track piece is split in half, as the race starts in the middle of it.
        segment = new List<Piece>();

        for(int i = switchIndices[switchIndices.Count - 1]; i < raceData.Track.Pieces.Length; i++)
        {
            segment.Add(raceData.Track.Pieces[i]);
        }

        for(int i = 0; i < switchIndices[0]; i++)
        {
            segment.Add(raceData.Track.Pieces[i]);
        }

        switchSegments.Add(segment.ToArray());

        // Work through each piece in each track segment and add up the length of it.
        List<float[]> laneLengths = new List<float[]>();

        foreach(Piece[] currentSegment in switchSegments)
        {
            List<float> currentSegmentLengths = new List<float>();
            for(int i = 0; i < laneCount; i++)
            {
                currentSegmentLengths.Add(0);
            }

            foreach (Piece piece in currentSegment)
            {
                for(int i = 0; i < laneCount; i++)
                {
                    currentSegmentLengths[i] += LanePieceLength(piece, raceData.Track.Lanes[i]);
                }
            }

            laneLengths.Add(currentSegmentLengths.ToArray());
        }

        raceData.Track.TrackSegmentLengths = laneLengths.ToArray();
        raceData.Track.SwitchIndices = switchIndices.ToArray();
    }

    /// <summary>
    /// Return the length of specific lane in a track piece.
    /// </summary>
    /// <param name="piece">The track piece.</param>
    /// <param name="lane">The lane.</param>
    /// <returns>The length.</returns>
    private static float LanePieceLength(Piece piece, Lane lane)
    {        
        if(piece.Length != 0)
        {
            return piece.Length;
        }

        float distanceFromCenter = piece.Radius - lane.DistanceFromCenter;
        float travelDistance = ((2.0f * (float)Math.PI) * distanceFromCenter) * ((piece.Angle) / 360.0f);

        return travelDistance;
    }

    public static float distanceToReduceSpeed(float velocity, float targetVelocity, float drag)
    {
        float distance = 0;
        int tickCount = 0;

        while(velocity > targetVelocity)
        {
            distance += velocity;
            velocity -= (velocity * velocity) * drag;
            tickCount += 1;
        }

        return distance;
    }
}
